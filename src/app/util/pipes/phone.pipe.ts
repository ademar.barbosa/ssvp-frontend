import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'fone'})
export class PhonePipe implements PipeTransform {
  transform(val, args) {
    const tamanho = val.length;
    let valorFormatado;

    if (tamanho === 10) {
      valorFormatado = '(' + val.substr(0, 2) + ') ' + val.substr(2, 4) + '-' + val.substr(6, 8);
    } else {
      valorFormatado = '(' + val.substr(0, 2) + ') ' + val.substr(2, 5) + '-' + val.substr(7, 9);
    }

    return valorFormatado;
  }
}
