import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StorageService } from '../service/infra/storage.service';
import { AlertaService } from '../service/alerta.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private alertaService: AlertaService,
        private storage: StorageService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.storage.getLocalUser();
        if (currentUser) {
            if (route.data.roles && route.data.roles.indexOf(currentUser.authorities) === -1) {
                this.alertaService.error('Acesso não autorizado!', true);
                this.storage.setLocalUser(null);
                this.router.navigate(['/']);
                return false;
            }
            return true;
        }
        this.router.navigate(['/'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}