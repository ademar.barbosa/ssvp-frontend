import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpErrorHandler } from 'src/app/util/http-error-handler.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DialogoComponent } from './dialogo.component';
import { MatButtonModule, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UnidadeDTO } from '../model/unidade.dto';

describe('DialogoComponent', () => {
  let component: DialogoComponent;
  let fixture: ComponentFixture<DialogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FlexLayoutModule,
        MatButtonModule,
        MatDialogModule
      ],
      declarations: [ DialogoComponent ],
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close: (dialogResult: any) => { }
        }
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            // tslint:disable-next-line:no-unused-expression
            data: (unidade: any) => { new UnidadeDTO(null, null, ''); }
          }
        },
        { provide: HttpErrorHandler }
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.onNoClick();
    expect(component).toBeTruthy();
  });

});
