import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UnidadeDTO } from '../model/unidade.dto';
import { ConsultaUnidadesComponent } from '../unidades/consultaunidades/consulta-unidades.component';

@Component({
    selector: 'app-dialogo',
    templateUrl: './dialogo.component.html',
    styleUrls: ['./dialogo.component.scss']
  })
export class DialogoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogoComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  delete(unidade: any) {
  }


}
