import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { DetalheUnidadeComponent } from './unidades/detalheunidade/detalhe-unidade.component';
import { InclusaoUnidadesComponent } from './unidades/inclusaounidades/inclusao-unidades.component';
import { EdicaoUnidadesComponent } from './unidades/edicaounidades/edicao-unidades.component';
import { ConsultaUnidadesComponent } from './unidades/consultaunidades/consulta-unidades.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AlertaComponent } from './alerta/alerta.component';
import { MatCardModule, MatFormFieldModule, MatDialogModule, MatButtonModule, MatSelectModule, MatOptionModule } from '@angular/material';
import { MatAutocompleteModule, MatIconModule, MatPaginatorModule, MatTableModule, MatInputModule, MatSortModule } from '@angular/material';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
import { FlexLayoutModule } from '@angular/flex-layout';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientModule,
        FormsModule,
        NgxMaskModule.forRoot({clearIfNotMatch: false, showMaskTyped: true}),
        NgxPhoneMaskBrModule,
        MatCardModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        MatSelectModule,
        MatOptionModule,
        MatAutocompleteModule,
        FlexLayoutModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatInputModule,
        MatSortModule
      ],
      declarations: [
        AppComponent,
        DetalheUnidadeComponent,
        NavComponent,
        AlertaComponent,
        InclusaoUnidadesComponent,
        EdicaoUnidadesComponent,
        ConsultaUnidadesComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ssvp'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('ssvp');
  });

  it('should have an app-nav', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-nav'));
  });
});
