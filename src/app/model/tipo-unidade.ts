export enum TipoUnidade {
    CONFERENCIA = 'Conferência',
    CONSELHO_PARTICULAR = 'Conselho Particular',
    CONSELHO_CENTRAL = 'Conselho Central',
    CONSELHO_METROPOLITANO = 'Conselho Metropolitano',
    REGIAO = 'Região',
    CONSELHO_NACIONAL = 'Conselho Nacional'
}
