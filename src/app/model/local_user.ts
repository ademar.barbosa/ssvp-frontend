export interface LocalUser {
    token: string;
    email: string;
    authorities: string[];
}
