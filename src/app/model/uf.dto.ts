import { Injectable } from '@angular/core';
import { CidadeDTO } from './cidade.dto';

export class UfDTO {

    constructor(
        public ufSigla: string,
        public ufNome: string,
        public cidades?: CidadeDTO[]) {}

}
