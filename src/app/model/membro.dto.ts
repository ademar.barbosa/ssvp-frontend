import { SituacaoMembro } from "./situacao-membro";

export class MembroDTO {
  constructor(
    public membroId: number,
    public nome: string,
    public email: string,
    public telefone: string,
    public dataNascimento: string,
    public situacaoMembro: SituacaoMembro,
    public tipoMembro: string,
    public perfil: string,
    public senha: string
  ) {}
}
