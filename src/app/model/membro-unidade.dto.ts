
export class MembroUnidadeDTO {

    constructor(
        public unidadeId: number,
        public membroId: number
    ) {}
}
