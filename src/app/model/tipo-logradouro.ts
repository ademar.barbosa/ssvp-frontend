export enum TipoLogradouro {
    RUA = 'Rua',
    AVENIDA = 'Avenida',
    PRACA = 'Praça'
}
