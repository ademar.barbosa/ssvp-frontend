import { TipoLogradouro } from './tipo-logradouro';
import { CidadeDTO } from './cidade.dto';
import { UfDTO } from './uf.dto';

export class EnderecoDTO {

    constructor(
       public tipoLogradouro: TipoLogradouro,
       public logradouro: string,
       public numero: string,
       public complemento: string,
       public bairro: string,
       public cep: string,
       public cidade: CidadeDTO,
       public uf: UfDTO,
       public referenciaEndereco: string
    ) {}

}
