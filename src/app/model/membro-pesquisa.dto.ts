import { SituacaoMembro } from './situacao-membro';

export class MembroPesquisaDTO {
  constructor(
    public membroId: number,
    public nome: string,
    public email: string,
    public telefone: string,
    public dataNascimento: string,
    public situacaoMembro: SituacaoMembro,
    public estaAssociado: boolean
  ) {}
}