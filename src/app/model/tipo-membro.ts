export enum TipoMembro {
  PRESIDENTE = 'Presidente',
	VICE_PRESIDENTE = 'Vice Presidente',
	SECRETARIO = 'Secretário(a)',
	VICE_SECRETARIO = 'Vice Secretário(a)',
	TESOUREIRO = 'Tesoureiro(a)',
	VICE_TESOUREIRO = 'Vice Tesoureiro(a)'
}
