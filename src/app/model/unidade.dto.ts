import { TipoUnidade } from './tipo-unidade';
import { EnderecoDTO } from './endereco.dto';
import { ReuniaoDTO } from './reuniao.dto';
import { ContatoDTO } from './contato.dto';

export class UnidadeDTO {

    constructor(
        public id: number,
        public unidadeId: number,
        public unidadeNome: string,
        public email?: string,
        public telefone?: string,
        public tipoUnidade?: TipoUnidade,
        public endereco?: EnderecoDTO,
        public reuniao?: ReuniaoDTO,
        public unidadeSuperior?: UnidadeDTO
    ) {}
}
