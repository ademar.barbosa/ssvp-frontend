import { UfDTO } from './uf.dto';

export class CidadeDTO {

    constructor(
        public cidadeId: number,
        public cidadeNome: string
        ) {}

}
