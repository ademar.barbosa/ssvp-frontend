export enum SituacaoMembro {
  ATIVO = 'Ativo',
	AFASTADO = 'Afastado',
	FALECIDO = 'Falecido'
}
