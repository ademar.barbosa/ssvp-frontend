import { TipoContato } from './tipo-contato';

export class ContatoDTO {
    constructor(
        public nome: string,
        public telefone: string,
        public celular: string,
        public tipoContato: TipoContato
    ) {}
}
