export class ReuniaoDTO {
    constructor(
        public diaSemana: string,
        public horario: string
    ) {}
}