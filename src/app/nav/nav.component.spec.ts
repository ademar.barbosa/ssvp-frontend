import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavComponent } from './nav.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageService } from '../service/infra/storage.service';
import { Router } from '@angular/router';
import { AuthService } from '../service/infra/auth.service';
import { CredenciaisDTO } from '../model/credenciais.dto';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  const credsAdm: CredenciaisDTO = {
    email: 'ademar.barbosa@gmail.com',
    senha: '123'
  };

  const credsConsulta: CredenciaisDTO = {
    email: 'consulta@ssvp.com',
    senha: '123'
  };

  const credsConsultaUnidade: CredenciaisDTO = {
    email: 'consulta_unidade@ssvp.com',
    senha: '123'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ],
      declarations: [ NavComponent ],
      providers: [
        StorageService,
        AuthService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be logged out', () => {
    const service: StorageService = TestBed.get(StorageService);
    component.logout();
    expect(service.getLocalUser()).toBeNull();

    const injector = TestBed;
    const router = injector.get(Router);
    expect(router.url).toBe('/');
  });

  it('should not be loggged', () => {
    expect(component.isLogged()).toBeFalsy();
  });

  it('should be loggged', (done: DoneFn) => {
    const service: AuthService = TestBed.get(AuthService);
    const storageService: StorageService = TestBed.get(StorageService);
    service.authenticate(credsAdm).subscribe(
      response => {
        service.successfulLogin(response.headers.get('Authorization'));
        expect(storageService.getLocalUser().token).toBeDefined();
        expect(component.isLogged()).toBeTruthy();
        component.logout();
        done();
      });
    }
  );

  it('should have access to menu as admin role', (done: DoneFn) => {
    const service: AuthService = TestBed.get(AuthService);
    const storageService: StorageService = TestBed.get(StorageService);
    service.authenticate(credsAdm).subscribe(
      response => {
        service.successfulLogin(response.headers.get('Authorization'));
        expect(storageService.getLocalUser().token).toBeDefined();
        expect(component.hasAccessMenu()).toBeTruthy();
        component.logout();
        done();
      });
    }
  );

  it('should have access to menu as consulta role', (done: DoneFn) => {
    const service: AuthService = TestBed.get(AuthService);
    const storageService: StorageService = TestBed.get(StorageService);
    service.authenticate(credsConsulta).subscribe(
      response => {
        service.successfulLogin(response.headers.get('Authorization'));
        expect(storageService.getLocalUser().token).toBeDefined();
        expect(component.hasAccessMenu()).toBeTruthy();
        component.logout();
        done();
      });
    }
  );

  it('should have access to menu as consulta unidade role', (done: DoneFn) => {
    const service: AuthService = TestBed.get(AuthService);
    const storageService: StorageService = TestBed.get(StorageService);
    service.authenticate(credsConsultaUnidade).subscribe(
      response => {
        service.successfulLogin(response.headers.get('Authorization'));
        expect(storageService.getLocalUser().token).toBeDefined();
        expect(component.hasAccessMenu()).toBeTruthy();
        component.logout();
        done();
      });
    }
  );

  it('should not have access to menu', () => {
    if (component.isLogged()) {
      component.logout();
    }
    expect(component.hasAccessMenu()).toBeFalsy();
  });

});
