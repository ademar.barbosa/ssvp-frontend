import { Component, OnInit } from '@angular/core';
import { StorageService } from '../service/infra/storage.service';
import { AuthService } from '../service/infra/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  appTitle = 'SSVP';

  constructor(protected route: ActivatedRoute,
    protected router: Router,
    private auth: AuthService,
    private storage: StorageService) { }

  ngOnInit() {
  }

  logout() {
    this.auth.logout();
    this.storage.setLocalUser(null);
    this.voltarLogin();
  }

  isLogged() {
    if (this.storage.getLocalUser() != null) {
      return true;
    }
    return false;
  }

  hasAccessMenu() {
    const currentUser = this.storage.getLocalUser();
    if (currentUser !== null) {
      if (currentUser.authorities.indexOf('ROLE_ADMIN') >= 0
        || currentUser.authorities.indexOf('ROLE_CONSULTA') >= 0
        || currentUser.authorities.indexOf('ROLE_CONSULTA_UNIDADE') >= 0) {
        return true;
      }
    }
    return false;
  }

  hasAccessAdmConsulta() {
    const currentUser = this.storage.getLocalUser();
    if (currentUser !== null) {
      if (currentUser.authorities.indexOf('ROLE_ADMIN') >= 0
        || currentUser.authorities.indexOf('ROLE_CONSULTA') >= 0) {
        return true;
      }
    }
    return false;
  }

  public voltarLogin() {
    this.router.navigate(['/']);
  }

}
