import { Component, OnInit, Injector } from '@angular/core';
import { MembrosAbstract } from '../membros-abstract';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertaService } from 'src/app/service/alerta.service';
import { MembroDTO } from 'src/app/model/membro.dto';

@Component({
  selector: 'app-edicao-membros',
  templateUrl: './edicao-membros.component.html',
  styleUrls: ['./edicao-membros.component.scss']
})
export class EdicaoMembrosComponent extends MembrosAbstract implements OnInit {

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector,
    private alertaService: AlertaService) {
      super(route, router, injector);
    }

    ngOnInit() {
      this.getMembro();
    }

    private getMembro() {
      const id = +this.route.snapshot.paramMap.get('id');
      console.log('component->getMembro');
      console.log(id);
      this.membroService.getMembroPorId(id)
        .subscribe(membro => {
          this.membro = membro;
          console.log(this.membro.perfil);
        }, err => this.tratarErroGetMembro());
    }

    tratarErroGetMembro() {
      this.alertaService.error('Não foi possível localizar o membro solicitado!', true);
      this.voltarConsulta();
    }

    onSubmit() {
      this.prepararSalvarMembro();
      this.membroService.atualizarMembro(this.membro)
        .subscribe(() => {
          this.alertaService.success('Membro incluído com sucesso!', true);
          this.voltarConsulta();
        }, err => this.tratarErroAlterar());
    }

    tratarErroAlterar() {
      this.alertaService.error('Ocorreu um erro ao alterar o membro!', false);
    }

}
