import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicaoMembrosComponent } from './edicao-membros.component';

describe('EdicaoMembrosComponent', () => {
  let component: EdicaoMembrosComponent;
  let fixture: ComponentFixture<EdicaoMembrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdicaoMembrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicaoMembrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
