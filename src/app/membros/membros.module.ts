import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule, MatIconModule, MatPaginatorModule, MatFormFieldModule } from '@angular/material';
import { MatInputModule, MatSortModule, MatDialogModule, MatButtonModule, MatSelectModule, MatOptionModule } from '@angular/material';
import { MatCardModule, MatAutocompleteModule, MatProgressSpinnerModule, MatTabsModule } from '@angular/material';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ConsultaMembrosComponent } from './consulta-membros/consulta-membros.component';
import { InclusaoMembrosComponent } from './inclusao-membros/inclusao-membros.component';
import { EdicaoMembrosComponent } from './edicao-membros/edicao-membros.component';
import { DetalheMembrosComponent } from './detalhe-membros/detalhe-membros.component';
import { PhonePipeModule } from '../util/pipes/phone-pipe.module';

@NgModule({
  declarations: [
    ConsultaMembrosComponent,
    InclusaoMembrosComponent,
    EdicaoMembrosComponent,
    DetalheMembrosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    NgxMaskModule.forRoot({ clearIfNotMatch: false, showMaskTyped: true }),
    NgxPhoneMaskBrModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    PhonePipeModule
  ]
})
export class MembrosModule {}
