import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InclusaoMembrosComponent } from './inclusao-membros.component';

describe('InclusaomembrosComponent', () => {
  let component: InclusaoMembrosComponent;
  let fixture: ComponentFixture<InclusaoMembrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InclusaoMembrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InclusaoMembrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
