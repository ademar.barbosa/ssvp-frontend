import { Component, OnInit, Injector } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertaService } from "src/app/service/alerta.service";
import { MembrosAbstract } from "../membros-abstract";

@Component({
  selector: "app-inclusao-membros",
  templateUrl: "./inclusao-membros.component.html",
  styleUrls: ["./inclusao-membros.component.scss"]
})
export class InclusaoMembrosComponent extends MembrosAbstract
  implements OnInit {
  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector,
    private alertaService: AlertaService
  ) {
    super(route, router, injector);
  }

  ngOnInit() {}

  onSubmit() {
    this.prepararSalvarMembro();
    this.membroService.inserirMembro(this.membro).subscribe(
      () => {
        this.alertaService.success("Membro incluído com sucesso!", true);
        this.voltarConsulta();
      },
      err => this.tratarErroIncluir(err)
    );
  }

  tratarErroIncluir(err) {
    let mensagemErro = "";
    err.error.errors.forEach(element => {
      mensagemErro += element.message;
    });
    this.alertaService.error(
      "Ocorreu um erro ao incluir o membro! " + mensagemErro,
      false
    );
  }
}
