import { ActivatedRoute, Router } from "@angular/router";
import { Injector } from "@angular/core";
import { MembroService } from "../service/membro.service";
import { MembroDTO } from "../model/membro.dto";
import { TipoMembro } from "../model/tipo-membro";
import { SituacaoMembro } from "../model/situacao-membro";
import { Perfil } from "../model/perfil";

export abstract class MembrosAbstract {
  protected membroService: MembroService;

  membro: MembroDTO = new MembroDTO(
    null,
    "",
    "",
    "",
    null,
    null,
    null,
    null,
    null
  );

  situacaoMembros: SituacaoMembro[] = [
    SituacaoMembro.ATIVO,
    SituacaoMembro.AFASTADO,
    SituacaoMembro.FALECIDO
  ];

  tipoMembros: TipoMembro[] = [
    TipoMembro.PRESIDENTE,
    TipoMembro.SECRETARIO,
    TipoMembro.TESOUREIRO,
    TipoMembro.VICE_PRESIDENTE,
    TipoMembro.VICE_SECRETARIO,
    TipoMembro.VICE_TESOUREIRO
  ];

  perfis: Perfil[] = [Perfil.ADMIN, Perfil.CONSULTA, Perfil.CONSULTA_UNIDADE];

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector
  ) {
    this.membroService = injector.get(MembroService);
  }

  protected prepararSalvarMembro() {
    // TODO: Implementar este método.
  }

  public voltarConsulta() {
    this.router.navigate(["/consultamembros"]);
  }

  protected abrirEdicao(id: number) {
    if (id !== null) {
      this.router.navigate(["/unidade", { id: id }]);
    }
  }

  protected abrirInclusao() {
    this.router.navigate(["/unidade"]);
  }
}
