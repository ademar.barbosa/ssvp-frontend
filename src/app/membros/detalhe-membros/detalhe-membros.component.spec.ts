import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheMembrosComponent } from './detalhe-membros.component';

describe('DetalheMembrosComponent', () => {
  let component: DetalheMembrosComponent;
  let fixture: ComponentFixture<DetalheMembrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalheMembrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheMembrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
