import { Component, OnInit, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertaService } from 'src/app/service/alerta.service';
import { MembrosAbstract } from '../membros-abstract';

@Component({
  selector: 'app-detalhe-membros',
  templateUrl: './detalhe-membros.component.html',
  styleUrls: ['./detalhe-membros.component.scss']
})
export class DetalheMembrosComponent extends MembrosAbstract implements OnInit {

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector,
    private alertaService: AlertaService) {
      super(route, router, injector);
    }

    ngOnInit() {
      this.getMembro();
    }

    private getMembro() {
      const id = +this.route.snapshot.paramMap.get('id');
      this.membroService.getMembroPorId(id)
        .subscribe(membro => {
          this.membro = membro;
        }, err => this.tratarErroGetMembro());
    }

    tratarErroGetMembro() {
      this.alertaService.error('Não foi possível localizar o membro solicitado!', true);
      this.voltarConsulta();
    }

}
