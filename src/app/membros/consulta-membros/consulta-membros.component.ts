import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MembroService } from 'src/app/service/membro.service';
import { AlertaService } from 'src/app/service/alerta.service';
import { StorageService } from 'src/app/service/infra/storage.service';
import { MembroDTO } from 'src/app/model/membro.dto';
import { DialogoComponent } from 'src/app/dialogo/dialogo.component';
import { PhonePipe } from 'src/app/util/pipes/phone.pipe';

@Component({
  selector: 'app-consulta-membros',
  templateUrl: './consulta-membros.component.html',
  styleUrls: ['./consulta-membros.component.scss']
})
export class ConsultaMembrosComponent implements OnInit {

  membros: any;
  displayedColumns: string[] = ['numero', 'nome', 'email', 'telefone', 'situacaoMembro', 'acoes'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isLoadingResults = true;

  constructor(
    private membroService: MembroService,
    public dialog: MatDialog,
    private alertaService: AlertaService,
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.membroService.getMembros().subscribe(membros => {
      this.membros = new MatTableDataSource<MembroDTO>(membros);
      this.membros.paginator = this.paginator;
      this.definirOrdenacaoConsulta();
      this.definirFiltroConsulta();
      this.isLoadingResults = false;
    });
  }

  definirOrdenacaoConsulta(): any {
    this.membros.sort = this.sort;
  }

  definirFiltroConsulta() {
    this.membros.filterPredicate = (data: MembroDTO, filter: string) =>
        data.nome.trim().toLowerCase().indexOf(filter) !== -1
        || data.email.trim().toLowerCase().indexOf(filter) !== -1
        || data.telefone.trim().toLowerCase().indexOf(filter) !== -1
        || data.situacaoMembro.trim().toLowerCase().indexOf(filter) !== -1;
  }

  openDialog(membro: MembroDTO): void {
    const dialogRef = this.dialog.open(DialogoComponent, {
      height: '210px',
      width: '480px',
      data: membro
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.delete(membro);
        this.alertaService.success('Membro excluído com sucesso!', false);
      }
    });
  }

  applyFilter(filterValue: string) {
    this.membros.filter = filterValue.trim().toLowerCase();

    if (this.membros.paginator) {
      this.membros.paginator.firstPage();
    }
  }

  delete(membro: MembroDTO) {
    this.membroService.excluirMembro(membro).subscribe(u => {
      let listMembros = this.membros.data as MembroDTO[];
      listMembros = listMembros.filter((unid: MembroDTO) => unid !== membro);
      this.membros.data = listMembros;
    }, err => this.tratarErroExcluir(err));
  }

  tratarErroExcluir(err: any): void {
    this.alertaService.error(err.error.message, false);
  }

  public hasAccessAdm(): Boolean {
    const currentUser = this.storage.getLocalUser();

    if (currentUser === null) {
      return false;
    } else {
      if (currentUser.authorities.indexOf('ROLE_ADMIN') >= 0) {
        return true;
      } else {
        return false;
      }
    }
  }

}
