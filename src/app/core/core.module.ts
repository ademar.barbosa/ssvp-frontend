import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from '../service/in-memory-data.service';
import { MatProgressSpinnerModule } from '@angular/material';

@NgModule({
    imports: [
        FormsModule,
        BrowserModule,
        HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { passThruUnknownUrl : true }),
    ],
    exports: [
        BrowserModule,
        MatProgressSpinnerModule
    ]
})
export class CoreModule { }
