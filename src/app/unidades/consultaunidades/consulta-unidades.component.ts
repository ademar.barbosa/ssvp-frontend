import { Component, OnInit, ViewChild } from '@angular/core';
import { UnidadeService } from '../../service/unidade.service';
import { UnidadeDTO } from '../../model/unidade.dto';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { DialogoComponent } from '../../dialogo/dialogo.component';
import { AlertaService } from '../../service/alerta.service';
import { StorageService } from 'src/app/service/infra/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './consulta-unidades.component.html',
  styleUrls: ['./consulta-unidades.component.scss']
})
export class ConsultaUnidadesComponent implements OnInit {

  unidades: any;
  displayedColumns: string[] = ['unidadeId', 'unidadeNome', 'tipoUnidade', 'endereco.bairro', 'endereco.cidade.cidadeNome', 'acoes'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  isLoadingResults = true;

  constructor(
    private unidadeService: UnidadeService,
    public dialog: MatDialog,
    private alertaService: AlertaService,
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.unidadeService.getUnidades().subscribe(unidades => {
      this.unidades = new MatTableDataSource<UnidadeDTO>(unidades);
      this.unidades.paginator = this.paginator;
      this.definirOrdenacaoConsulta();
      this.definirFiltroConsulta();
      this.isLoadingResults = false;
    });
  }

  definirOrdenacaoConsulta(): any {
    this.unidades.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'endereco.bairro': return item.endereco.bairro;
        case 'endereco.cidade.cidadeNome': return item.endereco.cidade.cidadeNome;
        default: return item[property];
      }
    };
    this.unidades.sort = this.sort;
  }

  definirFiltroConsulta() {
    this.unidades.filterPredicate = (data: UnidadeDTO, filter: string) =>
        data.unidadeNome.trim().toLowerCase().indexOf(filter) !== -1
        || data.endereco.bairro.trim().toLowerCase().indexOf(filter) !== -1
        || data.endereco.cidade.cidadeNome.trim().toLowerCase().indexOf(filter) !== -1
        || data.tipoUnidade.trim().toLowerCase().indexOf(filter) !== -1;
  }

  openDialog(unidade: UnidadeDTO): void {
    const dialogRef = this.dialog.open(DialogoComponent, {
      height: '210px',
      width: '480px',
      data: unidade
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.delete(unidade);
        this.alertaService.success('Unidade excluída com sucesso!', false);
      }
    });
  }

  applyFilter(filterValue: string) {
    this.unidades.filter = filterValue.trim().toLowerCase();

    if (this.unidades.paginator) {
      this.unidades.paginator.firstPage();
    }
  }

  delete(unidade: UnidadeDTO) {
    this.unidadeService.excluirUnidade(unidade).subscribe(u => {
      let listUnidades = this.unidades.data as UnidadeDTO[];
      listUnidades = listUnidades.filter((unid: UnidadeDTO) => unid !== unidade);
      this.unidades.data = listUnidades;
    }, err => this.tratarErroExcluir());
  }
  
  tratarErroExcluir(): void {
    this.alertaService.error('Ocorreu um erro ao excluir a unidade!', false);
  }

  public hasAccessAdm(): Boolean {
    const currentUser = this.storage.getLocalUser();

    if (currentUser === null) {
      return false;
    } else {
      if (currentUser.authorities.indexOf('ROLE_ADMIN') >= 0) {
        return true;
      } else {
        return false;
      }
    }
  }

}
