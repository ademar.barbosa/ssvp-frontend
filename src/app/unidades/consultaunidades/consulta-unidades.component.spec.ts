import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConsultaUnidadesComponent } from './consulta-unidades.component';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from '../../service/data.service';
import { MatCardModule, MatFormFieldModule, MatDialogModule, MatSelectModule, MatButtonModule, MatTableModule, MatTableDataSource } from '@angular/material';
import { MatIconModule, MatPaginatorModule, MatSortModule, MatInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterTestingModule } from '@angular/router/testing';
import { UnidadeService } from 'src/app/service/unidade.service';
import { HttpErrorHandler } from 'src/app/util/http-error-handler.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UnidadeDTO } from 'src/app/model/unidade.dto';
import { AuthGuard } from 'src/app/util/auth-guard';
import { AuthService } from 'src/app/service/infra/auth.service';
import { StorageService } from 'src/app/service/infra/storage.service';
import { CredenciaisDTO } from 'src/app/model/credenciais.dto';

describe('ConsultaUnidadesComponent', () => {
  let component: ConsultaUnidadesComponent;
  let fixture: ComponentFixture<ConsultaUnidadesComponent>;

  const credsAdm: CredenciaisDTO = {
    email: 'ademar.barbosa@gmail.com',
    senha: '123'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        FlexLayoutModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatSortModule,
        MatInputModule
      ],
      providers: [
        UnidadeService,
        HttpErrorHandler,
        AuthService,
        StorageService
      ],
      declarations: [ ConsultaUnidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaUnidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
