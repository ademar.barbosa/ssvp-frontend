import { UnidadeDTO } from '../model/unidade.dto';
import { EnderecoDTO } from '../model/endereco.dto';
import { CidadeDTO } from '../model/cidade.dto';
import { UfDTO } from '../model/uf.dto';
import { ReuniaoDTO } from '../model/reuniao.dto';
import { TipoUnidade } from '../model/tipo-unidade';
import { TipoLogradouro } from '../model/tipo-logradouro';
import { UnidadeService } from '../service/unidade.service';
import { EstadoService } from '../service/estado.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Injector } from '@angular/core';
import { StorageService } from '../service/infra/storage.service';

export abstract class UnidadesAbstract {

    protected unidadeService: UnidadeService;
    protected estadoService: EstadoService;

    unidade: UnidadeDTO =   new UnidadeDTO(null, null, '', '', '', null,
                                         new EnderecoDTO(null,
                                         '', '', '', '', '',
                                         new CidadeDTO(null, ''),
                                         new UfDTO(null, '', null), ''),
                                         new ReuniaoDTO('', ''),
                                         new UnidadeDTO(null, null, '')
                                        );
  tipoUnidades: TipoUnidade[] =         [TipoUnidade.CONFERENCIA,
                                         TipoUnidade.CONSELHO_PARTICULAR,
                                         TipoUnidade.CONSELHO_CENTRAL,
                                         TipoUnidade.CONSELHO_METROPOLITANO,
                                         TipoUnidade.REGIAO,
                                         TipoUnidade.CONSELHO_NACIONAL
                                        ];
  tipoLogradouros: TipoLogradouro[] =   [TipoLogradouro.RUA,
                                         TipoLogradouro.AVENIDA,
                                         TipoLogradouro.PRACA
                                        ];
  cidades: CidadeDTO[] =                [];
  estados: UfDTO[] =                    [];
  unidadesSuperiores: UnidadeDTO[] =    [];
  unidadesSuperioresComplete: UnidadeDTO[] =    [];

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector
  ) {
    this.unidadeService = injector.get(UnidadeService);
    this.estadoService = injector.get(EstadoService);
  }

  protected getUnidadesSuperiores() {
    this.unidadeService.getUnidadesSuperiores(this.unidade.tipoUnidade).subscribe(unidades => {
        this.unidadesSuperiores = unidades;
        this.unidadesSuperioresComplete = unidades;
    });
  }

  public carregarCidades(ufSigla: string) {
    if (ufSigla != null) {
      this.estadoService.getCidades(ufSigla).subscribe(cidades => this.cidades = cidades);
    } else {
      this.cidades = null;
    }
  }

  public carregarUnidadesSuperiores(tipoUnidade: string) {
    this.getUnidadesSuperiores();
  }

  protected getEstados() {
    this.estadoService.getEstados().subscribe(estados => {
        this.estados = estados;
    });
  }

  protected prepararSalvarUnidade() {
    this.setUnidadeSuperior();
    this.setCidadeNome();
    this.getNomeCidadeById(this.unidade.endereco.cidade.cidadeId);
  }

  protected setUnidadeSuperior() {
    const unidade = this.getUnidadeById(this.unidade.unidadeSuperior.unidadeId);
    if (unidade != null) {
      unidade.email = null;
      unidade.endereco = null;
      unidade.reuniao = null;
      unidade.telefone = null;
      unidade.tipoUnidade = null;
      unidade.unidadeSuperior = null;
      this.unidade.unidadeSuperior = unidade;
    }
  }

  protected getUnidadeById(unidadeId: number): UnidadeDTO {
    if (unidadeId == null) {
        return null;
    } else {
        const unidade: UnidadeDTO = this.unidadesSuperiores.filter(x => x.unidadeId === unidadeId)
                                                .reduce(x => x.unidadeId === unidadeId ? x : null);
        return unidade;
    }
  }

  protected setCidadeNome() {
    this.unidade.endereco.cidade.cidadeNome =
        this.getNomeCidadeById(this.unidade.endereco.cidade.cidadeId);
  }

  protected getNomeCidadeById(cidadeId: number): string {
    if (cidadeId == null) {
        return null;
    } else {
        const cidade: CidadeDTO = this.cidades.filter(x => x.cidadeId === cidadeId).reduce(x => x.cidadeId === cidadeId ? x : null);
        return cidade.cidadeNome;
    }
  }

  public voltarConsulta() {
    this.router.navigate(['/consultaunidades']);
  }

  protected abrirEdicao(id: number) {
    if (id !== null) {
      this.router.navigate(['/unidade', { id: id }]);
    }
  }

  protected abrirInclusao() {
    this.router.navigate(['/unidade']);
  }

}
