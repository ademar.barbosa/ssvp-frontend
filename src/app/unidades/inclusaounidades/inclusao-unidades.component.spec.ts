import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InclusaoUnidadesComponent } from './inclusao-unidades.component';
import { FormsModule } from '@angular/forms';
import { HttpErrorHandler } from 'src/app/util/http-error-handler.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule, MatFormFieldModule, MatDialogModule, MatButtonModule, MatTableModule } from '@angular/material';
import { MatIconModule, MatPaginatorModule } from '@angular/material';
import { MatInputModule, MatOptionModule, MatSelectModule, MatSortModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';

describe('InclusaoUnidadesComponent', () => {
  let component: InclusaoUnidadesComponent;
  let fixture: ComponentFixture<InclusaoUnidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatInputModule,
        FormsModule,
        NgxMaskModule.forRoot({clearIfNotMatch: false, showMaskTyped: true}),
        NgxPhoneMaskBrModule,
        MatOptionModule,
        MatSelectModule,
        MatIconModule,
        MatPaginatorModule
      ],
      declarations: [ InclusaoUnidadesComponent ],
      providers: [
        HttpErrorHandler
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InclusaoUnidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
