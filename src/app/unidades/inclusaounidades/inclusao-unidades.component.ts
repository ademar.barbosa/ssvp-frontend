import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertaService } from '../../service/alerta.service';
import { UnidadesAbstract } from '../unidades-abstract';

@Component({
  selector: 'app-inclusao-unidade',
  templateUrl: './inclusao-unidades.component.html',
  styleUrls: ['./inclusao-unidades.component.scss']
})
export class InclusaoUnidadesComponent extends UnidadesAbstract implements OnInit {

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector,
    private alertaService: AlertaService) {
      super(route, router, injector);
    }

  ngOnInit() {
    this.getUnidadesSuperiores();
    this.carregarCidades(null);
    this.getEstados();
  }

  onSubmit() {
    this.prepararSalvarUnidade();
    this.unidadeService.inserirUnidade(this.unidade)
      .subscribe(() => {
        this.alertaService.success('Unidade incluída com sucesso!', true);
        this.voltarConsulta();
      }, err => this.tratarErroIncluir());
  }

  tratarErroIncluir() {
    this.alertaService.error('Ocorreu um erro ao incluir a unidade!', false);
  }

  doFilter() {
    this.unidadesSuperioresComplete =
      this.unidadesSuperiores.filter(x => x.unidadeNome.toLowerCase().includes(this.unidade.unidadeSuperior.unidadeNome));
  }

}

// Criar serviço de alerta.
// https://stackoverflow.com/questions/44526390/angular-material-create-alert-similar-to-bootstrap-alerts

// Dynamic Mask
// https://stackblitz.com/edit/angular-cicjvx?file=app%2Fapp.component.ts
