import { NgModule } from '@angular/core';
import { DetalheUnidadeComponent } from './detalheunidade/detalhe-unidade.component';
import { EdicaoUnidadesComponent } from './edicaounidades/edicao-unidades.component';
import { InclusaoUnidadesComponent } from './inclusaounidades/inclusao-unidades.component';
import { ConsultaUnidadesComponent } from './consultaunidades/consulta-unidades.component';
import { MatCardModule, MatFormFieldModule, MatProgressSpinnerModule, MatTabsModule } from '@angular/material';
import { MatInputModule, MatSortModule, MatTableModule, MatIconModule, MatPaginatorModule } from '@angular/material';
import { MatDialogModule, MatButtonModule, MatSelectModule, MatOptionModule, MatAutocompleteModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../app-routing.module';
import { PhonePipeModule } from '../util/pipes/phone-pipe.module';

@NgModule({
  declarations: [
    DetalheUnidadeComponent,
    EdicaoUnidadesComponent,
    InclusaoUnidadesComponent,
    ConsultaUnidadesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    NgxMaskModule.forRoot({clearIfNotMatch: false, showMaskTyped: true}),
    NgxPhoneMaskBrModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatAutocompleteModule,
    FlexLayoutModule, 
    MatProgressSpinnerModule,
    MatTabsModule,
    PhonePipeModule
  ]
})
export class UnidadesModule { }
