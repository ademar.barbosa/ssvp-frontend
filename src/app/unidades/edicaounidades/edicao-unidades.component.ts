import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertaService } from '../../service/alerta.service';
import { UnidadesAbstract } from '../unidades-abstract';
import { UnidadeDTO } from 'src/app/model/unidade.dto';
import { MatTabChangeEvent, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MembroDTO } from 'src/app/model/membro.dto';
import { MembroService } from 'src/app/service/membro.service';
import { MembroPesquisaDTO } from 'src/app/model/membro-pesquisa.dto';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edicao-unidade',
  templateUrl: './edicao-unidades.component.html',
  styleUrls: ['./edicao-unidades.component.scss']
})
export class EdicaoUnidadesComponent extends UnidadesAbstract implements OnInit {

  pesquisaMembroText: string;
  tabSelected = 0;

  membrosAssociados: any;
  displayedColumnsAssociados: string[] = ['numero', 'nome', 'email', 'telefone', 'situacaoMembro', 'acoes'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  membrosPesquisados: any;
  displayedColumnsPesquisados: string[] = ['numero', 'nome', 'email', 'telefone', 'situacaoMembro', 'acoes'];
  @ViewChild(MatPaginator) paginatorPesquisados: MatPaginator;
  @ViewChild(MatSort) sortPesquisados: MatSort;

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected injector: Injector,
    protected membroService: MembroService,
    private alertaService: AlertaService) {
      super(route, router, injector);
    }

  ngOnInit() {
    this.getUnidadesSuperiores();
    this.getUnidade();
    this.getEstados();
  }

  private getUnidade() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.unidadeService.getUnidade(id)
      .subscribe(unidade => {
        if (unidade.unidadeSuperior === null) {
          unidade.unidadeSuperior = new UnidadeDTO(null, null, '');
        }
        this.unidade = unidade;
        this.carregarCidades(this.unidade.endereco.uf.ufSigla);
        this.carregarUnidadesSuperiores(this.unidade.tipoUnidade);
      }, err => this.tratarErroGetUnidade());
  }

  tratarErroGetUnidade() {
    this.alertaService.error('Não foi possível localizar a unidade solicitada!', true);
    this.voltarConsulta();
  }

  tratarErroSalvarUnidade() {
    this.alertaService.error('Ocorreu um erro ao atualizar a unidade!', false);
  }

  onSubmit() {
    this.prepararSalvarUnidade();
    this.unidadeService.atualizarUnidade(this.unidade)
      .subscribe(() => {
        this.alertaService.success('Unidade alterada com sucesso!', true);
          this.voltarConsulta();
        }, err => this.tratarErroSalvarUnidade());
  }

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    console.log('tabChangeEvent => ', tabChangeEvent);
    console.log('index => ', tabChangeEvent.index);
    if (tabChangeEvent.index === 1) {
      this.carregarMembrosUnidade(this.unidade.unidadeId);
    }
  }

  carregarMembrosUnidade(unidadeId: number): any {
    this.unidadeService.getMembroUnidade(unidadeId)
      .subscribe(membros => {
        console.log(membros);
        this.membrosAssociados = new MatTableDataSource<MembroDTO>(membros);
        this.membrosAssociados.paginator = this.paginator;
        this.definirOrdenacaoConsulta();
      });
  }

  onSubmitPesquisar(associarMembroForm: NgForm) {
    this.membroService.getMembroUnidadePorNome(this.pesquisaMembroText, this.unidade.tipoUnidade)
      .subscribe(membros => {
        this.membrosPesquisados = new MatTableDataSource<MembroPesquisaDTO>(membros);
        this.membrosPesquisados.paginator = this.paginatorPesquisados;
        this.definirOrdenacaoPesquisa();
      });
  }
  
  definirOrdenacaoConsulta(): any {
    this.membrosAssociados.sort = this.sort;
  }

  definirOrdenacaoPesquisa(): any {
    this.membrosPesquisados.sort = this.sortPesquisados;
  }

  associarMembro(membro: MembroPesquisaDTO, associarMembroForm: NgForm) {
    let pesquisaAux = this.pesquisaMembroText;
    this.unidadeService.associarMembro(membro.membroId, this.unidade.unidadeId).subscribe(() => {
      if (this.pesquisaMembroText != '') {
        this.onSubmitPesquisar(associarMembroForm);
      } else {
        this.pesquisaMembroText = '';
      }
      associarMembroForm.resetForm();
      this.pesquisaMembroText = pesquisaAux;
      this.carregarMembrosUnidade(this.unidade.unidadeId);
      this.alertaService.success('Membro associado com sucesso!', false);
    }, err => this.tratarErroAssociarMembro());
  }

  tratarErroAssociarMembro() {
    this.alertaService.error('Ocorreu um erro ao associar o membro à unidade!', false);
  }

  tratarErroDesassociarMembro() {
    this.alertaService.error('Ocorreu um erro ao desassociar o membro!', false);
  }

  desassociarMembro(membro: MembroPesquisaDTO, associarMembroForm: NgForm) {
    let pesquisaAux = this.pesquisaMembroText;
    this.unidadeService.desassociarMembro(membro.membroId, this.unidade.unidadeId).subscribe(() => {
      if (this.pesquisaMembroText != '') {
        this.onSubmitPesquisar(associarMembroForm);
      } else {
        this.pesquisaMembroText = '';
      }
      associarMembroForm.resetForm();
      this.pesquisaMembroText = pesquisaAux;
      this.carregarMembrosUnidade(this.unidade.unidadeId);
      this.alertaService.success('Membro desassociado com sucesso!', false);
    }, err => this.tratarErroDesassociarMembro());
  }

}
