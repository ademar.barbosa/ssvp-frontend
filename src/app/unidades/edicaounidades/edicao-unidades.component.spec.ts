import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EdicaoUnidadesComponent } from './edicao-unidades.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatFormFieldModule, MatDialogModule, MatButtonModule, MatTableModule, MatIconModule } from '@angular/material';
import { MatPaginatorModule, MatSortModule, MatInputModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
import { HttpErrorHandler } from 'src/app/util/http-error-handler.service';

describe('EdicaoUnidadesComponent', () => {
  let component: EdicaoUnidadesComponent;
  let fixture: ComponentFixture<EdicaoUnidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        FlexLayoutModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatSortModule,
        MatInputModule,
        FormsModule,
        NgxMaskModule.forRoot({clearIfNotMatch: false, showMaskTyped: true}),
        NgxPhoneMaskBrModule,
        MatOptionModule,
        MatSelectModule,
        MatIconModule
      ],
      declarations: [
        EdicaoUnidadesComponent
      ],
      providers: [
        HttpErrorHandler
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicaoUnidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
