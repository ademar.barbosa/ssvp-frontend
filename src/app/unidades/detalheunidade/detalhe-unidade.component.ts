import { Component, OnInit, Injector } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UnidadesAbstract } from '../unidades-abstract';
import { UnidadeDTO } from 'src/app/model/unidade.dto';
import { AlertaService } from 'src/app/service/alerta.service';

@Component({
  selector: 'app-detalhe-unidade',
  templateUrl: './detalhe-unidade.component.html',
  styleUrls: ['./detalhe-unidade.component.scss']
})
export class DetalheUnidadeComponent extends UnidadesAbstract implements OnInit {

  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    private alertaService: AlertaService,
    protected injector: Injector) {
      super(route, router, injector);
    }

  ngOnInit() {
    this.getUnidade();
  }

  private getUnidade() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.unidadeService.getUnidade(id)
      .subscribe(unidade => {
        if (unidade.unidadeSuperior === null) {
          unidade.unidadeSuperior = new UnidadeDTO(null, null, '',);
        }
        this.unidade = unidade;
      }, err => this.tratarErroGetUnidade());
  }

  tratarErroGetUnidade() {
    this.alertaService.error('Não foi possível localizar a unidade solicitada!', true);
    this.voltarConsulta();
  }

}
