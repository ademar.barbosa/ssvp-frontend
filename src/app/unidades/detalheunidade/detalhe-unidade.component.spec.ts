import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheUnidadeComponent } from './detalhe-unidade.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule, MatFormFieldModule, MatDialogModule, MatButtonModule, MatTableModule, MatIconModule } from '@angular/material';
import { MatPaginatorModule, MatSortModule, MatInputModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPhoneMaskBrModule } from 'ngx-phone-mask-br';
import { HttpErrorHandler } from 'src/app/util/http-error-handler.service';
import { ConsultaUnidadesComponent } from '../consultaunidades/consulta-unidades.component';
import { AuthGuard } from 'src/app/util/auth-guard';

describe('DetalheUnidadeComponent', () => {
  let component: DetalheUnidadeComponent;
  let fixture: ComponentFixture<DetalheUnidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatDialogModule,
        MatButtonModule,
        FlexLayoutModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatSortModule,
        MatInputModule,
        FormsModule,
        NgxMaskModule.forRoot({clearIfNotMatch: false, showMaskTyped: true}),
        NgxPhoneMaskBrModule
      ],
      declarations: [
        DetalheUnidadeComponent,
        ConsultaUnidadesComponent
      ],
      providers: [
        HttpErrorHandler
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheUnidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
