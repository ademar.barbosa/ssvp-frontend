import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AlertaComponent } from './alerta.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertaService } from '../service/alerta.service';

describe('AlertaComponent', () => {
    let component: AlertaComponent;
    let fixture: ComponentFixture<AlertaComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
          imports: [
            RouterTestingModule,
            BrowserModule,
            BrowserAnimationsModule,
            HttpClientModule
          ],
          declarations: [ AlertaComponent ],
          providers: [
            AlertaService
          ]
        })
        .compileComponents();
      }));

      beforeEach(() => {
        fixture = TestBed.createComponent(AlertaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });

      it('should create', () => {
        expect(component).toBeTruthy();
      });

      it('should show message', () => {
        const service: AlertaService = TestBed.get(AlertaService);
        service.error('Ocorreu um erro ao incluir a unidade!', false);
        expect(component).toBeTruthy();
      });

});
