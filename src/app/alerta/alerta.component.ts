import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertaService } from '../service/alerta.service';

@Component({
    selector: 'app-alerta',
    templateUrl: 'alerta.component.html',
    styleUrls: ['./alerta.component.css']
})
export class AlertaComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    message: any;

    constructor(private alertaService: AlertaService) { }

    ngOnInit() {
        this.subscription = this.alertaService.getMessage().subscribe(message => {
            this.message = message;
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
