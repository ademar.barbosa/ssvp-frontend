import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ConsultaUnidadesComponent } from "./unidades/consultaunidades/consulta-unidades.component";
import { DetalheUnidadeComponent } from "./unidades/detalheunidade/detalhe-unidade.component";
import { EdicaoUnidadesComponent } from "./unidades/edicaounidades/edicao-unidades.component";
import { InclusaoUnidadesComponent } from "./unidades/inclusaounidades/inclusao-unidades.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./util/auth-guard";
import { ConsultaMembrosComponent } from "./membros/consulta-membros/consulta-membros.component";
import { InclusaoMembrosComponent } from "./membros/inclusao-membros/inclusao-membros.component";
import { EdicaoMembrosComponent } from "./membros/edicao-membros/edicao-membros.component";
import { DetalheMembrosComponent } from "./membros/detalhe-membros/detalhe-membros.component";

const routes: Routes = [
  {
    path: "consultaunidades",
    component: ConsultaUnidadesComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN", "ROLE_CONSULTA", "ROLE_CONSULTA_UNIDADE"] }
  },
  {
    path: "detalhe/:id",
    component: DetalheUnidadeComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN", "ROLE_CONSULTA", "ROLE_CONSULTA_UNIDADE"] }
  },
  {
    path: "unidade",
    component: InclusaoUnidadesComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN"] }
  },
  {
    path: "unidade/:id",
    component: EdicaoUnidadesComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN"] }
  },
  {
    path: "consultamembros",
    component: ConsultaMembrosComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN", "ROLE_CONSULTA"] }
  },
  {
    path: "incluir_membro",
    component: InclusaoMembrosComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN"] }
  },
  {
    path: "editar_membro/:id",
    component: EdicaoMembrosComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN"] }
  },
  {
    path: "detalhar_membro/:id",
    component: DetalheMembrosComponent,
    canActivate: [AuthGuard],
    data: { roles: ["ROLE_ADMIN", "ROLE_CONSULTA", "ROLE_CONSULTA_UNIDADE"] }
  },
  {
    path: "",
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
