import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './service/in-memory-data.service';
import { MatProgressSpinnerModule, MatDialogModule,  } from '@angular/material';
import { HttpErrorHandler } from './util/http-error-handler.service';
import { DialogoComponent } from './dialogo/dialogo.component';
import { AlertaComponent } from './alerta/alerta.component';
import { AuthInterceptorProvider } from './interceptor/auth-interceptor';
import { UnidadesModule } from './unidades/unidades.module';
import { MembrosModule } from './membros/membros.module';
import { LoginModule } from './login/login.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';

enableProdMode();
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    DialogoComponent,
    AlertaComponent
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    MatDialogModule,
    UnidadesModule,
    MembrosModule,
    LoginModule
  ],
  providers: [ HttpErrorHandler, MatDialogModule, AuthInterceptorProvider ],
  bootstrap: [ AppComponent ],
  exports: [ ],
  entryComponents: [ DialogoComponent ]
})
export class AppModule { }
