import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HandleError, HttpErrorHandler } from '../util/http-error-handler.service';
import { API_CONFIG } from '../config/api.config';
import { MembroDTO } from '../model/membro.dto';
import { tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MembroPesquisaDTO } from '../model/membro-pesquisa.dto';
import { TipoUnidade } from '../model/tipo-unidade';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class MembroService {

  private membrosUrl = `${API_CONFIG.baseUrl}/membros`;
  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('MembroService');
  }

  getMembros() {
    return this.http.get<MembroDTO[]>(this.membrosUrl);
  }

  getMembro(email: string): Observable<MembroDTO> {
    const url = `${this.membrosUrl}/${email}/`;
    return this.http.get<MembroDTO>(url).pipe(
      catchError(this.handleError<MembroDTO>(`getMembro email=${email}`))
    );
  }

  getMembroPorId(id: number): Observable<MembroDTO> {
    console.log('service->getMembroPorId');
    console.log(id);
    const url = `${this.membrosUrl}/${id}/`;
    return this.http.get<MembroDTO>(url).pipe(
      catchError(this.handleError<MembroDTO>(`getMembroPorId id=${id}`))
    );
  }

  getMembroUnidadePorNome(nome: string, tipoUnidade: TipoUnidade): Observable<MembroPesquisaDTO[]> {
    const url = `${this.membrosUrl}/find/${nome}/${tipoUnidade}`;
    return this.http.get<MembroPesquisaDTO[]>(url).pipe(
      catchError(this.handleError<MembroPesquisaDTO[]>(`getMembroUnidadePorNome id=${nome}`))
    );
  }

  inserirMembro(membro: MembroDTO) {
    const url = `${this.membrosUrl}`;
    return this.http.post<MembroDTO>(url, membro, httpOptions)
      .pipe(
        tap((m) => console.log(m.nome)),
        catchError(this.handleError('inserirMembro', membro))
      );
  }

  atualizarMembro(membro: MembroDTO) {
    const url = `${this.membrosUrl}`;
    return this.http.put(url, membro, httpOptions).pipe(
      catchError(this.handleError<any>('atualizarMembro = e-mail=' + membro.email))
    );
  }

  excluirMembro(membro: MembroDTO | number): Observable<MembroDTO> {
    const id = typeof membro === 'number' ? membro : membro.membroId;
    const url = `${this.membrosUrl}/${id}`;
    return this.http.delete<MembroDTO>(url, httpOptions).pipe(
      catchError(this.handleError<MembroDTO>('excluirMembro membro=' + id))
    );
  }

}
