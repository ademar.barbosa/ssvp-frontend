import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UfDTO } from '../model/uf.dto';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CidadeDTO } from '../model/cidade.dto';
import { HttpErrorHandler, HandleError } from '../util/http-error-handler.service';
import { API_CONFIG } from '../config/api.config';

@Injectable({
    providedIn: 'root'
})
export class EstadoService {

    private estadosUrl = `${API_CONFIG.baseUrl}/estados`;
    private handleError: HandleError;

    constructor(private http: HttpClient,
        httpErrorHandler: HttpErrorHandler) {
        this.handleError = httpErrorHandler.createHandleError('EstadoService');
    }

    getEstados(): Observable<UfDTO[]> {
        return this.http.get<UfDTO[]>(`${this.estadosUrl}?ordenadoPor=ufNome&direcao=ASC`);
    }

    getEstado(ufSigla: string): Observable<CidadeDTO[]> {
        return this.getEstados().pipe(
            map((x: UfDTO[]) => x.filter((uf: UfDTO) => uf.ufSigla === ufSigla)),
            map(this.retornarCidades),
            catchError(this.handleError<CidadeDTO[]>(`getEstado id=${ufSigla}`))
        );
    }

    retornarCidades(ufList: UfDTO[]): CidadeDTO[] {
        let cidades: CidadeDTO[] = [];
        ufList.forEach(estado => {
            cidades = estado.cidades;
        });

        return cidades;
    }

    getCidades(ufSigla: string): Observable<CidadeDTO[]> {
        const url = `${this.estadosUrl}/${ufSigla}/cidades`;
        return this.http.get<CidadeDTO[]>(url);
    }

}
