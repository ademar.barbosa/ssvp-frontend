import { InMemoryDbService } from 'angular-in-memory-web-api';
import { TipoUnidade } from '../model/tipo-unidade';
import { TipoLogradouro } from '../model/tipo-logradouro';
import { TipoContato } from '../model/tipo-contato';
import { UnidadeDTO } from '../model/unidade.dto';
import { CidadeDTO } from '../model/cidade.dto';
import { UfDTO } from '../model/uf.dto';

export class InMemoryDataService implements InMemoryDbService {

    // Prioritário:
    // Adicionar e-mail, lista de membros (hoje é contatos),
    // departamentos (ECAFO, CCA, CJ, DENOR, DECON) (quando for qualquer tipo de conselho) e paróquia.

    // Não prioritário:
    // Guardar histórico de mandato, inserir comentários para as conferências (diagnóstico da unidade) - manter histórico com data e hora,
    // cadastro de famílias assitidas (para conferências).

    createDb() {
        const unidades: UnidadeDTO[] = [
            {
                id: 1,
                unidadeId: 1,
                unidadeNome: 'Conferência 1',
                email: 'conf1@ssvp.com.br',
                telefone: '(31) 98585-0950',
                tipoUnidade: TipoUnidade.CONFERENCIA,
                endereco: {
                    tipoLogradouro: TipoLogradouro.RUA,
                    logradouro: 'Santa Ana',
                    numero: '362',
                    complemento: 'Apto 502',
                    bairro: 'Água Branca',
                    cep: '30730-470',
                    cidade: {
                        cidadeId: 2,
                        cidadeNome: 'Contagem'
                    },
                    uf: {
                        ufSigla: 'MG',
                        ufNome: 'Minas Gerais'
                    },
                    referenciaEndereco: ''
                },
                reuniao: {
                    diaSemana: 'Segunda-feira',
                    horario: '20:00'
                },
                unidadeSuperior: {
                    id: 6,
                    unidadeId: 6,
                    unidadeNome: 'Conselho Particular 1'
                }
            },
            {
                id: 2,
                unidadeId: 2,
                unidadeNome: 'Conselho Central 1',
                email: 'cent1@ssvp.com.br',
                telefone: '(31) 90525-1758',
                tipoUnidade: TipoUnidade.CONSELHO_CENTRAL,
                endereco: {
                    tipoLogradouro: TipoLogradouro.RUA,
                    logradouro: 'Ibertioga',
                    numero: '205',
                    complemento: 'Apto 502',
                    bairro: 'Água Branca',
                    cep: '32372-190',
                    cidade: {
                        cidadeId: 2,
                        cidadeNome: 'Contagem'
                    },
                    uf: {
                        ufSigla: 'MG',
                        ufNome: 'Minas Gerais'
                    },
                    referenciaEndereco: ''
                },
                reuniao: {
                    diaSemana: 'Quinta-feira',
                    horario: '19:30'
                },
                unidadeSuperior: {
                    id: 3,
                    unidadeId: 3,
                    unidadeNome: 'Conselho Metropolitano 1'
                }
            },
            {
                id: 3,
                unidadeId: 3,
                unidadeNome: 'Conselho Metropolitano 1',
                email: 'metrop1@ssvp.com.br',
                telefone: '(31) 93165-1301',
                tipoUnidade: TipoUnidade.CONSELHO_METROPOLITANO,
                endereco: {
                    tipoLogradouro: TipoLogradouro.RUA,
                    logradouro: 'Ramos de Azevedo',
                    numero: '550',
                    complemento: 'Apto 501',
                    bairro: 'Monsenhor Messias',
                    cep: '30730-470',
                    cidade: {
                        cidadeId: 1,
                        cidadeNome: 'Belo Horizonte'
                    },
                    uf: {
                        ufSigla: 'MG',
                        ufNome: 'Minas Gerais'
                    },
                    referenciaEndereco: ''
                },
                reuniao: {
                    diaSemana: 'Quarta-feira',
                    horario: '18:00'
                },
                unidadeSuperior: {
                    id: 5,
                    unidadeId: 5,
                    unidadeNome: 'Regional Minas Gerais'
                }
            },
            {
                id: 4,
                unidadeId: 4,
                unidadeNome: 'Conselho Nacional 1',
                email: 'cons1@ssvp.com.br',
                telefone: '(31) 98022-7996',
                tipoUnidade: TipoUnidade.CONSELHO_NACIONAL,
                endereco: {
                    tipoLogradouro: TipoLogradouro.RUA,
                    logradouro: 'Pará de Minas',
                    numero: '500',
                    complemento: '',
                    bairro: 'Padre Eustáquio',
                    cep: '30700-000',
                    cidade: {
                        cidadeId: 1,
                        cidadeNome: 'Belo Horizonte'
                    },
                    uf: {
                        ufSigla: 'MG',
                        ufNome: 'Minas Gerais'
                    },
                    referenciaEndereco: ''
                },
                reuniao: {
                    diaSemana: 'Sábado',
                    horario: '16:10'
                },
                unidadeSuperior: {
                    id: null,
                    unidadeId: null,
                    unidadeNome: ''
                }},
            {
                id: 5,
                unidadeId: 5,
                unidadeNome: 'Regional Minas Gerais',
                email: 'reg1@ssvp.com.br',
                telefone: '(31) 97571-0474',
                tipoUnidade: TipoUnidade.REGIAO,
                endereco: {
                    tipoLogradouro: TipoLogradouro.RUA,
                    logradouro: 'Saúde',
                    numero: '79',
                    complemento: 'Apto 204 A',
                    bairro: 'Minas Brasil',
                    cep: '30730-470',
                    cidade: {
                        cidadeId: 1,
                        cidadeNome: 'Belo Horizonte'
                    },
                    uf: {
                        ufSigla: 'MG',
                        ufNome: 'Minas Gerais'
                    },
                    referenciaEndereco: ''
                },
                reuniao: {
                    diaSemana: 'Domingo',
                    horario: '08:00'
                },
                unidadeSuperior: {
                    id: 4,
                    unidadeId: 4,
                    unidadeNome: 'Conselho Nacional 1'
                }
            },
            {
                id: 6,
                unidadeId: 6,
                unidadeNome: 'Conselho Particular 1',
                email: 'part1@ssvp.com.br',
                telefone: '(31) 98585-1989',
                tipoUnidade: TipoUnidade.CONSELHO_PARTICULAR,
                endereco: {
                    tipoLogradouro: TipoLogradouro.RUA,
                    logradouro: 'Santa Ana',
                    numero: '362',
                    complemento: 'Apto 502',
                    bairro: 'Água Branca',
                    cep: '30730-470',
                    cidade: {
                        cidadeId: 2,
                        cidadeNome: 'Contagem'
                    },
                    uf: {
                        ufSigla: 'MG',
                        ufNome: 'Minas Gerais'
                    },
                    referenciaEndereco: ''
                },
                reuniao: {
                    diaSemana: 'Segunda-feira',
                    horario: '20:00'
                },
                unidadeSuperior: {
                    id: 2,
                    unidadeId: 2,
                    unidadeNome: 'Conselho Central 1'
                }
            }
        ];
        const cidades: CidadeDTO[] = [
            {cidadeId: 1, cidadeNome: 'Belo Horizonte'},
            {cidadeId: 2, cidadeNome: 'Contagem'}
        ];

        const estados: UfDTO[] = [
            {ufSigla: 'MG', ufNome: 'Minas Gerais', cidades: [
                {cidadeId: 1, cidadeNome: 'Belo Horizonte'},
                {cidadeId: 2, cidadeNome: 'Contagem'}]}
        ];

        return {unidades, cidades, estados};
    }

    genId(unidades: UnidadeDTO[]): number {
        const nextVal = unidades.length > 0 ? Math.max(...unidades.map(unidade => unidade.id)) + 1 : 6;
        return nextVal;
    }

    // genId<T extends UnidadeDTO | CidadeDTO | UfDTO>(myTable: T[]): number {
    //     const nextVal = myTable.length > 0 ? Math.max(...myTable.map(t => t.id)) + 1 : 6;
    //     console.log(nextVal);

    //     myTable.id = nextVal;
    //     return nextVal;
    // }

    // genID
    // https://stackoverflow.com/questions/40146811/multiple-collections-in-angular-in-memory-web-api

}
