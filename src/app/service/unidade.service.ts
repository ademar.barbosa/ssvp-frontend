import { Injectable } from '@angular/core';
import { UnidadeDTO } from '../model/unidade.dto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HandleError, HttpErrorHandler } from '../util/http-error-handler.service';
import { API_CONFIG } from '../config/api.config';
import { TipoUnidade } from '../model/tipo-unidade';
import { MembroDTO } from '../model/membro.dto';
import { MembroUnidadeDTO } from '../model/membro-unidade.dto';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class UnidadeService {

  private unidadesUrl = `${API_CONFIG.baseUrl}/unidades`;
  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('UnidadeService');
  }

  getUnidades() {
    return this.http.get<UnidadeDTO[]>(this.unidadesUrl);
  }

  getUnidadesSuperiores(tipoUnidade: string): Observable<UnidadeDTO[]> {
    if (tipoUnidade != null && tipoUnidade !== '') {
      const tipoUnidadeSuperior = this.getTipoUnidadeSuperior(tipoUnidade);

      if (tipoUnidadeSuperior != null) {
        return this.http.get<UnidadeDTO[]>(`${this.unidadesUrl}?tipoUnidade=${tipoUnidadeSuperior}`);
      } else {
        return new Observable<UnidadeDTO[]>();
      }
    } else {
      return this.http.get<UnidadeDTO[]>(`${this.unidadesUrl}`);
    }
  }

  getTipoUnidadeSuperior(tipoUnidade: string): string {
    switch(tipoUnidade) {
      case TipoUnidade.CONFERENCIA: {
         return TipoUnidade.CONSELHO_PARTICULAR;
      }
      case TipoUnidade.CONSELHO_PARTICULAR: {
         return TipoUnidade.CONSELHO_CENTRAL;
      }
      case TipoUnidade.CONSELHO_CENTRAL: {
        return TipoUnidade.CONSELHO_METROPOLITANO;
      }
      case TipoUnidade.CONSELHO_METROPOLITANO: {
        return TipoUnidade.REGIAO;
      }
      case TipoUnidade.REGIAO: {
        return TipoUnidade.CONSELHO_NACIONAL;
      }
      default: {
        return null;
      }
   }
  }

  getUnidade(id: number): Observable<UnidadeDTO> {
    const url = `${this.unidadesUrl}/${id}`;
    return this.http.get<UnidadeDTO>(url).pipe(
      catchError(this.handleError<UnidadeDTO>(`getUnidade id=${id}`))
    );
  }

  atualizarUnidade(unidade: UnidadeDTO) {
    const url = `${this.unidadesUrl}`;
    return this.http.put(url, unidade, httpOptions).pipe(
      catchError(this.handleError<any>('atualizarUnidade = id=' + unidade.unidadeId))
    );
  }

  inserirUnidade(unidade: UnidadeDTO) {
    const url = `${this.unidadesUrl}`;
    return this.http.post<UnidadeDTO>(url, unidade, httpOptions)
      .pipe(
        tap((unid) => unid.unidadeId = unid.id),
        catchError(this.handleError('inserirUnidade', unidade))
      );
  }

  excluirUnidade(unidade: UnidadeDTO | number): Observable<UnidadeDTO> {
    const id = typeof unidade === 'number' ? unidade : unidade.unidadeId;
    const url = `${this.unidadesUrl}/${id}`;
    return this.http.delete<UnidadeDTO>(url, httpOptions).pipe(
      catchError(this.handleError<UnidadeDTO>('excluirUnidade unidade=' + id))
    );
  }

  getMembroUnidade(id: number): Observable<MembroDTO[]> {
    const url = `${this.unidadesUrl}/${id}/membros`;
    return this.http.get<MembroDTO[]>(url).pipe(
      catchError(this.handleError<MembroDTO[]>(`getMembrosUnidade id=${id}`))
    );
  }

  associarMembro(membroId: number, unidadeId: number) {
    const url = `${this.unidadesUrl}/associarMembro`;
    const membroUnidade: MembroUnidadeDTO = new MembroUnidadeDTO(unidadeId, membroId);

    return this.http.put(url, membroUnidade, httpOptions).pipe(
      catchError(this.handleError<any>('associarMembro = unidadeId=' + membroUnidade.unidadeId +  + 'membroId='+ membroUnidade.membroId))
    );
  }

  desassociarMembro(membroId: number, unidadeId: number) {
    console.log('object');
    const url = `${this.unidadesUrl}/desassociarMembro/${unidadeId}/${membroId}`;
    console.log(url);

    return this.http.delete(url, httpOptions).pipe(
      catchError(this.handleError<any>('desassociarMembro = unidadeId=' + unidadeId +  + 'membroId='+ membroId))
    );
  }

}
