import { TestBed } from '@angular/core/testing';
import { UnidadeService } from './unidade.service';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpErrorHandler } from '../util/http-error-handler.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { API_CONFIG } from '../config/api.config';

describe('UnidadeService', () => {

    let service: UnidadeService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
      const spy = jasmine.createSpyObj('UnidadeService', ['getUnidades']);

      TestBed.configureTestingModule({
        imports: [
          HttpClientModule,
          RouterTestingModule,
          HttpClientTestingModule
        ],
        providers: [
          UnidadeService,
          { provide: HttpErrorHandler, useClass: HttpErrorHandler },
            HttpErrorHandler
          ]
      });
      service = TestBed.get(UnidadeService);
      httpTestingController = TestBed.get(HttpTestingController);
    });

    it('should call the google\'s map data', () => {
      // const mockGoogleMapData = {id: 1, country : 'United states of america', zipcode: '56743'};
      service.getUnidades().subscribe(data => {
        expect(data.length).toBeGreaterThan(0);
      });

      const req = httpTestingController.expectOne(`${API_CONFIG.baseUrl}/unidades`);

      console.log(req);

      expect(req.request.method).toBe('GET');

      // req.flush({
      //   mapData: mockGoogleMapData
      // });
    });

    // https://medium.com/@manivel45/angular-7-unit-testing-code-coverage-5c7a238315b6

  });
