import { TestBed } from '@angular/core/testing';
import { AlertaService } from './alerta.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('AlertaService', () => {
    beforeEach(() => TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [ AlertaService ]
    }));

    it('should be created', () => {
      const service: AlertaService = TestBed.get(AlertaService);
      expect(service).toBeTruthy();
    });
  });
