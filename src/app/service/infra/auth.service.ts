import { Injectable } from '@angular/core';
import { CredenciaisDTO } from '../../model/credenciais.dto';
import { HttpClient } from '@angular/common/http';
import { API_CONFIG } from '../../config/api.config';
import { LocalUser } from '../../model/local_user';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    jwtHelper: JwtHelperService = new JwtHelperService();

    constructor(
        public http: HttpClient,
        public storage: StorageService) {
    }

    authenticate(creds: CredenciaisDTO) {
        return this.http.post(`${API_CONFIG.baseUrl}/login`, creds,
            {
                observe: 'response',
                responseType: 'text'
            });
    }

    refreshToken() {
        return this.http.post(
            `${API_CONFIG.baseUrl}/auth/refresh_token`,
            {},
            {
                observe: 'response',
                responseType: 'text'
            });
    }

    heartbeat(localUser: LocalUser) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/auth/heartbeat`,
            localUser,
            {
                observe: 'response',
                responseType: 'text'
            });
    }

    successfulLogin(authorizationValue: string) {
        const tok = authorizationValue.substring(7);
        const authoritiesList = this.jwtHelper.decodeToken(authorizationValue)['authorities'];
        const user: LocalUser = {
            token: tok,
            email: this.jwtHelper.decodeToken(tok).sub,
            authorities: authoritiesList
        };
        this.storage.setLocalUser(user);
    }

    isTokenExpired(token: string): Boolean {
        return this.jwtHelper.isTokenExpired(token);
    }

    decodificarToken(token: string): Boolean {
        return this.jwtHelper.decodeToken(token);
    }

    logout() {
        this.storage.setLocalUser(null);
    }
}
