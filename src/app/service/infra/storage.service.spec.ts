import { TestBed } from '@angular/core/testing';
import { StorageService } from './storage.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalUser } from 'src/app/model/local_user';

describe('StorageService', () => {

    const localUser: LocalUser = {
      token: '',
      email: 'ademar.barbosa@gmail.com',
      authorities: []
    };

    beforeEach(() => TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [ StorageService ]
    }));

    it('should be created', () => {
      const service: StorageService = TestBed.get(StorageService);
      expect(service).toBeTruthy();
    });

    it('should localUser not be null',
      (done: DoneFn) => {
        const service: StorageService = TestBed.get(StorageService);
        service.setLocalUser(localUser);
        expect(service.getLocalUser().email).toBeDefined();
        done();
      }
    );

    it('should localUser not be null',
      (done: DoneFn) => {
        const service: StorageService = TestBed.get(StorageService);
        service.setLocalUser(localUser);
        expect(service.getLocalUser().email).toBeDefined();
        done();
      }
    );

    it('should remove localUser',
      (done: DoneFn) => {
        const service: StorageService = TestBed.get(StorageService);
        service.setLocalUser(null);
        expect(service.getLocalUser()).toBeNull();
        done();
      }
    );

  });
