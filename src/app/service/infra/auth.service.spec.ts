import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CredenciaisDTO } from 'src/app/model/credenciais.dto';
import { StorageService } from './storage.service';

describe('AuthService', () => {

    const creds: CredenciaisDTO = {
      email: 'ademar.barbosa@gmail.com',
      senha: '123'
    };

    beforeEach(() => { TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [ AuthService ]
    });
  });

    it('should be created', () => {
      const service: AuthService = TestBed.get(AuthService);
      expect(service).toBeTruthy();
    });

    it('should be authenticated',
      (done: DoneFn) => {
        const service: AuthService = TestBed.get(AuthService);
        service.authenticate(creds).subscribe(
          response => {
            expect(response.headers.get('Authorization')).toBeDefined();
            done();
        });
      }
    );

    it('should be successful login',
      (done: DoneFn) => {
        const service: AuthService = TestBed.get(AuthService);
        const storageService: StorageService = TestBed.get(StorageService);
        service.authenticate(creds).subscribe(
          response => {
            service.successfulLogin(response.headers.get('Authorization'));
            expect(storageService.getLocalUser().token).toBeDefined();
            done();
        });
      }
    );

    // it('should be heartBeat',
    //   (done: DoneFn) => {
    //     const service: AuthService = TestBed.get(AuthService);
    //     const storageService: StorageService = TestBed.get(StorageService);
    //     console.log(storageService.getLocalUser());
    //     service.heartbeat(storageService.getLocalUser())
    //         .subscribe(response => {
    //             service.successfulLogin(response.headers.get('Authorization'));
    //             expect(storageService.getLocalUser().token).toBeDefined();
    //         });
    //   }
    // );

  });
