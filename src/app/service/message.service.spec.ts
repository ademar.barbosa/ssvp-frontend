import { TestBed } from '@angular/core/testing';
import { MessageService } from './message.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('MessageService', () => {
    beforeEach(() => TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [ MessageService ]
    }));

    it('should be created', () => {
      const service: MessageService = TestBed.get(MessageService);
      expect(service).toBeTruthy();
    });
  });
