import { TestBed } from '@angular/core/testing';
import { EstadoService } from './estado.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpErrorHandler } from '../util/http-error-handler.service';

describe('EstadoService', () => {
    beforeEach(() => TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        EstadoService,
        HttpErrorHandler
      ]
    }));

    it('should be created', () => {
      const service: EstadoService = TestBed.get(EstadoService);
      expect(service).toBeTruthy();
    });
  });
