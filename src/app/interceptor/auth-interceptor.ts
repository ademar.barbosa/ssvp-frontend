import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { StorageService } from '../service/infra/storage.service';
import { API_CONFIG } from '../config/api.config';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { tap, filter, take, switchMap, catchError, finalize } from 'rxjs/operators';
import { AlertaService } from '../service/alerta.service';
import { AuthService } from '../service/infra/auth.service';
import { LocalUser } from '../model/local_user';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public storage: StorageService,
        private alertaService: AlertaService,
        private router: Router,
        public auth: AuthService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const localUser = this.storage.getLocalUser();

        const N = API_CONFIG.baseUrl.length;
        const requestToAPI = req.url.substring(0, N) === API_CONFIG.baseUrl;

        if (localUser != null) {
            const authReq = this.getToken(req, localUser);

            if (req.url.includes('/unidades')) {
                this.heartbeat(localUser);
            }
            return next.handle(authReq).pipe(
                tap((event: HttpEvent<any>) => {},
                (err: any) => {
                    this.tratarForbidden(err);
                })
            );
        } else {
            return next.handle(req).pipe(
                tap((event: HttpEvent<any>) => {},
                (err: any) => {
                    this.tratarForbidden(err);
                })
            );
        }

    }

    heartbeat(localUser: LocalUser): any {
        this.auth.heartbeat(localUser)
            .subscribe(response => {
                this.auth.successfulLogin(response.headers.get('Authorization'));
                localUser = this.storage.getLocalUser();
        });
    }

    private getToken(req: HttpRequest<any>, localUser: any) {
        return req.clone({headers: req.headers.set('Authorization', 'Bearer ' + localUser.token)});
    }

    private tratarForbidden(err: any): any {
        if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
                this.alertaService.error('Usuário e senha inválidos!', false);
                this.abrirLogin();
            }
            if (err.status === 403) {
                this.alertaService.error('Acesso não permitido!', false);
                this.abrirLogin();
            }
        }
    }

    private abrirLogin() {
        this.router.navigate(['/']);
    }

}

export const AuthInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
};
