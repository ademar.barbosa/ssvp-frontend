import { Component, OnInit } from '@angular/core';
import { CredenciaisDTO } from '../model/credenciais.dto';
import { AuthService } from '../service/infra/auth.service';
import { AlertaService } from '../service/alerta.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '../service/infra/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  credenciais: CredenciaisDTO = {
    email: '',
    senha: ''
  };

  constructor(public auth: AuthService,
    private alertaService: AlertaService,
    protected route: ActivatedRoute,
    protected router: Router,
    public storage: StorageService) { }

  ngOnInit() {

  }

  onSubmit() {
    this.auth.authenticate(this.credenciais)
      .subscribe(response => {
        this.auth.successfulLogin(response.headers.get('Authorization'));
        this.abrirConsultaUnidades();
      },
        error => {
          this.tratarErroLogin();
        });
  }

  renovarToken() {
    this.auth.refreshToken()
      .subscribe(
        response => {
          this.auth.successfulLogin(
            response.headers.get('Authorization'));
          this.abrirConsultaUnidades();
        },
        error => { });
  }

  tratarErroLogin() {
    this.alertaService.error('Ocorreu um erro ao autenticar o usuário!', false);
  }

  protected abrirConsultaUnidades() {
    this.router.navigate(['/consultaunidades']);
  }

  protected abrirLogin() {
    this.router.navigate(['/']);
  }

  logout() {
    this.auth.logout();
    this.abrirLogin();
  }

}
